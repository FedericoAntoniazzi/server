import { Calendar, ICalendar } from './calendar.model';

export class CalendarComponent {

  async get(courseName?: string, query?: any) {
    if (courseName) {
      query['courseName'] = courseName;
    }

    if (query['startDay'] || query['endDay'])
      query['date'] = {};

    if (query['startDay']) {
      query.date.$gte = new Date(query.startDay);
      delete query.startDay;
    }
    if (query['endDay']) {
      query.date.$lte = new Date(query.endDay);
      delete query.endDay;
    }

    return Calendar.aggregate([
      { $match: query },
      {
        $group: {
          _id: { $isoWeek: "$date" },
          days: { $push: "$$ROOT" }
        }
      },
      { $sort: { _id: 1 } }
    ]);

    // find(query).sort('-date');
  }

  async create(data: ICalendar[]) {
    await Calendar.deleteMany({});
    return Calendar.insertMany(data)
  }

  async courseList() {
    const items = await Calendar.find()
    return items.reduce((prev: String[], curr: ICalendar) => {
      if (!prev.includes(curr.courseName)) {
        prev.push(curr.courseName);
      }
      return prev;
    }, []);
  }
}
