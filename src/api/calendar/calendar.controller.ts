import { Request, Response, NextFunction } from 'express';
import { CalendarComponent } from './calendar.component';

export class CalendarController {
  protected component: CalendarComponent

  constructor() {
    this.component = new CalendarComponent();
  }

  get() {
    const self = this;
    return async function (req: Request, res: Response, next: NextFunction) {
      try {
        const item = await self.component.get(req.params.courseName, req.query);
        res.json(item);
      } catch (e) {
        next(e);
      }
    };
  }

  create() {
    const self = this;
    return async function (req: Request, res: Response, next: NextFunction) {
      try {
        const item = await self.component.create(req.body);
        res.json(item);
      } catch (e) {
        next(e);
      }
    };
  }

  courseList() {
    const self = this;
    return async function (req: Request, res: Response, next: NextFunction) {
      try {
        const item = await self.component.courseList();
        res.json(item);
      } catch (e) {
        next(e);
      }
    };
  }
}
