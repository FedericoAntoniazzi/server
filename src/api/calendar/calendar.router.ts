import { Router } from 'express';
import { CalendarController } from './calendar.controller';

export class CalendarRouter {
  controller: CalendarController
  router: Router;

  constructor() {
    this.controller = new CalendarController();
    this.router = Router({ mergeParams: true });
    this.define();
  }

  define() {
    this.router.get('/', this.controller.get());
    this.router.get('/courseList', this.controller.courseList());
    this.router.get('/:courseName', this.controller.get());
    this.router.post('/', this.controller.create());
  }
}
